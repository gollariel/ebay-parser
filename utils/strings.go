package utils

import (
	"unsafe"
)

// ByteSliceToString cast given bytes to string, without allocation memory
func ByteSliceToString(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

// Concat concatnates given strings
func Concat(strs ...string) string {
	if len(strs) == 0 {
		return ""
	}

	b := []byte(strs[0])
	for i := 1; i < len(strs); i++ {
		b = append(b, strs[i]...)
	}
	return ByteSliceToString(b)
}
