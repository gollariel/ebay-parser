package utils

import (
	"bytes"
	"strings"
	"testing"

	"bitbucket.org/gollariel/ebay-parser/utils/testutil"
)

func TestConcat(t *testing.T) {
	testcases := map[string]struct {
		inStrs []string
		outStr string
	}{
		"inStrs:empty":     {inStrs: []string{}, outStr: ""},
		"inStrs:nil":       {inStrs: nil, outStr: ""},
		"inStrs:non_empty": {inStrs: []string{"hello", " ", "world"}, outStr: "hello world"},
	}

	for k, c := range testcases {
		t.Run(k, func(t *testing.T) {
			str := Concat(c.inStrs...)
			testutil.TestingAssertEqual(t, str, c.outStr)
		})
	}
}

func TestByteSliceToString(t *testing.T) {
	testcases := map[string]struct {
		bytes []byte
		out   string
	}{
		"inStrs:empty":     {bytes: []byte{}, out: ""},
		"inStrs:nil":       {bytes: nil, out: ""},
		"inStrs:non_empty": {bytes: []byte{72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100, 33}, out: "Hello world!"},
	}

	for k, c := range testcases {
		t.Run(k, func(t *testing.T) {
			str := ByteSliceToString(c.bytes)
			testutil.TestingAssertEqual(t, str, c.out)
		})
	}
}

/*
BenchmarkConcat-4                       10000000               118 ns/op              56 B/op          3 allocs/op
BenchmarkJoin-4                         20000000                93.4 ns/op            32 B/op          1 allocs/op
BenchmarkSimpleConcatenation-4             10000            106570 ns/op          830538 B/op          7 allocs/op
BenchmarkBufferConcatenation-4          20000000               121 ns/op              96 B/op          2 allocs/op
BenchmarkConcatSmall-4                  30000000                45.5 ns/op             8 B/op          1 allocs/op
BenchmarkJoinSmall-4                    30000000                54.1 ns/op             3 B/op          1 allocs/op
BenchmarkSimpleConcatenationSmall-4       100000             73328 ns/op          461311 B/op          3 allocs/op
BenchmarkBufferConcatenationSmall-4     20000000                79.3 ns/op            67 B/op          2 allocs/op
BenchmarkByteSliceToString-4            50000000                21.6 ns/op            16 B/op          1 allocs/op
*/

var (
	strsForConcat      = []string{"hello", " ", "world", " ", "from", " ", "Golang"}
	strsForSmallConcat = []string{"a", "b", "c"}
	globalResForConcat string
)

func BenchmarkConcat(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		localRes = Concat(strsForConcat...)
	}
	globalResForConcat = localRes
}

func BenchmarkJoin(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		localRes = strings.Join(strsForConcat, "")
	}
	globalResForConcat = localRes
}

func BenchmarkSimpleConcatenation(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		for _, s := range strsForConcat {
			localRes += s
		}
	}
	globalResForConcat = localRes
}

func BenchmarkBufferConcatenation(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		buff := bytes.NewBuffer(nil)
		for _, s := range strsForConcat {
			_, _ = buff.WriteString(s)
		}
		localRes = buff.String()
	}
	globalResForConcat = localRes
}

func BenchmarkConcatSmall(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		localRes = Concat(strsForSmallConcat...)
	}
	globalResForConcat = localRes
}

func BenchmarkJoinSmall(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		localRes = strings.Join(strsForSmallConcat, "")
	}
	globalResForConcat = localRes
}

func BenchmarkSimpleConcatenationSmall(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		for _, s := range strsForSmallConcat {
			localRes += s
		}
	}
	globalResForConcat = localRes
}

func BenchmarkBufferConcatenationSmall(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		buff := bytes.NewBuffer(nil)
		for _, s := range strsForSmallConcat {
			_, _ = buff.WriteString(s)
		}
		localRes = buff.String()
	}
	globalResForConcat = localRes
}

func BenchmarkByteSliceToString(b *testing.B) {
	var localRes string
	for i := 0; i < b.N; i++ {
		localRes = ByteSliceToString([]byte{72, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100, 33})
	}
	globalResForConcat = localRes
}
