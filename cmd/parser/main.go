package main

import (
	"fmt"

	"bitbucket.org/gollariel/ebay-parser/fastlog"
	"bitbucket.org/gollariel/ebay-parser/parser"
)

func main() {
	category, err := parser.CollectCategory()
	if err != nil {
		fastlog.Fatal("Error parsing category", "err", err)
		return
	}
	fmt.Println(category.ToJSON())
}
