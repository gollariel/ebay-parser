package parser

import (
	"github.com/kelseyhightower/envconfig"
)

// EnvPrefix environment prefix for ebay category config
const EnvPrefix = "EBAY_CATEGORY"

// Config contains ebay category configs
type Config struct {
	URL         string `required:"true" split_words:"true"`
	Host        string `required:"true" split_words:"true" default:"https://www.ebay.com/b"`
	MaxDepth    int    `required:"true" split_words:"true" default:"10"` // max pages
	Parallelism int    `required:"true" split_words:"true" default:"4"`
	DomainGlob  string `required:"true" split_words:"true" default:"*"`
	Browser     string `required:"true" split_words:"true" default:"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"`
}

// GetConfigFromEnv return configs bases on environment variables
func GetConfigFromEnv() (*Config, error) {
	c := new(Config)
	err := envconfig.Process(EnvPrefix, c)
	return c, err
}
