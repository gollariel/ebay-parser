package parser

import (
	"encoding/json"
	"strings"
	"sync"

	"bitbucket.org/gollariel/ebay-parser/fastlog"
	"bitbucket.org/gollariel/ebay-parser/utils"
	"github.com/gocolly/colly/v2"
)

const buffer = 1000

// Category contains all items
type Category struct {
	Items []*Item
	mu    sync.RWMutex
}

// AddItem add item safety
func (c *Category) AddItem(i *Item) {
	c.mu.Lock()
	c.Items = append(c.Items, i)
	c.mu.Unlock()
}

// GetItems get all items
func (c *Category) GetItems() []*Item {
	c.mu.RLock()
	i := make([]*Item, len(c.Items))
	copy(i, c.Items)
	c.mu.RUnlock()
	return i
}

// ToJSON marshal data to json
func (c *Category) ToJSON() ([]byte, error) {
	i := c.GetItems()
	return json.Marshal(i)
}

// Item contains ebay item fields
type Item struct {
	Name       string
	Image      string
	URL        string
	Price      string
	Attributes map[string]string
}

// NewItem return new item
func NewItem(image, name, price, url string) *Item {
	return &Item{
		Image: image,
		Name:  name,
		Price: price,
		URL:   url,
	}
}

// AddAttribute add attribute
func (i *Item) AddAttribute(name string, value string) {
	if i.Attributes == nil {
		i.Attributes = map[string]string{}
	}
	i.Attributes[name] = value
}

// GetCollector return new collector
func GetCollector(cfg *Config) (*colly.Collector, error) {
	c := colly.NewCollector(
		colly.UserAgent(cfg.Browser),
		colly.Async(),
		colly.MaxDepth(cfg.MaxDepth),
	)
	return c, c.Limit(&colly.LimitRule{DomainGlob: cfg.DomainGlob, Parallelism: cfg.Parallelism})
}

// CollectCategory collect category
func CollectCategory() (*Category, error) {
	category := &Category{}
	ci, err := Collect()
	if err != nil {
		return category, err
	}
	for item := range ci {
		category.AddItem(item)
	}
	return category, err
}

// Collect collect items from page
func Collect() (chan *Item, error) {
	cfg, err := GetConfigFromEnv()
	if err != nil {
		return nil, err
	}

	c, err := GetCollector(cfg)
	if err != nil {
		return nil, err
	}

	ch := make(chan *Item, buffer)
	defer close(ch)

	c.OnHTML("li[class=\"s-item   \"]", func(e *colly.HTMLElement) {
		item := NewItem(e.ChildAttr("img[class=s-item__image-img]", "src"), e.ChildAttr("img[class=s-item__image-img]", "alt"), e.ChildText("span[class=s-item__price]"), e.ChildAttr("a[class=s-item__link]", "href"))
		e.ForEach("span[class=\"s-item__detail s-item__detail--secondary\"]", func(i int, element *colly.HTMLElement) {
			tmp := strings.Split(element.ChildAttr("span[class]", "class"), " ")
			if tmp[0] != "" {
				text := element.ChildText("span[class]")
				i := strings.Index(text, ":")
				if i != -1 {
					text = text[i+1:]
				}
				item.AddAttribute(strings.Replace(tmp[0], "s-item__", "", -1), strings.Trim(text, " "))
			}
		})
		ch <- item
	})
	c.OnHTML("nav[class=ebayui-pagination]", func(e *colly.HTMLElement) {
		url := strings.Replace(e.ChildAttr("a[rel=next]", "href"), "#", "", -1)
		if url == "" {
			return
		}
		err := e.Request.Visit(url)
		if err != nil {
			fastlog.Error("Error getting visit", "err", err)
			return
		}
	})
	c.OnError(func(response *colly.Response, err error) {
		if err != nil {
			fastlog.Error("Error getting response", "err", err)
			return
		}
	})

	err = c.Visit(utils.Concat(cfg.Host, "/", cfg.URL))
	if err != nil {
		return nil, err
	}
	c.Wait()

	return ch, nil
}
