module bitbucket.org/gollariel/ebay-parser

go 1.14

require (
	github.com/gocolly/colly/v2 v2.1.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.4.0
	go.uber.org/zap v1.15.0
)
